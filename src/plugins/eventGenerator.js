import {addDays, format} from "date-fns";

export default function generate(startDate = "2021-08-02", waterDaysIncluded = true){
    /*
    * { // Event example
          name: 'Teszt event', // dayName
          start: '2021-08-01',
          end: '2021-08-01',
          color: 'red',   // szín, nap típus
          timed: true,    // egész napos esemény
          placeNumber: 1, // hanyadik a sorban
        }
    * */
    let maxDaysOfDiet = (waterDaysIncluded ? 89 : 85)
    let dateOfStart = new Date(startDate);
    let dayTypes = Object.keys(DAY_TYPES);
    let events = [];
    let mainCounter = 0;
    let fourDayCounter = 0;
    let dayTypeCounter = 0;
    for (let mainCounter = 0; mainCounter < maxDaysOfDiet; mainCounter++) {
        let currentDayTye = dayTypes[dayTypeCounter];
        let currentDate = format(addDays(dateOfStart, (mainCounter+1)), "yyyy-MM-dd");
        if(mainCounter === 0){
            /*events.push(dateOfStart);*/
            events.push(createEvent(currentDayTye, currentDate, (mainCounter+1)))
        }
        else{
            if((mainCounter % 28 === 0) && waterDaysIncluded){
                dayTypeCounter = 0;
                fourDayCounter = fourDayCounter+1;
                events.push(createEvent(dayTypes[4], currentDate,(mainCounter+1)));
            }else{
                events.push(createEvent(currentDayTye, currentDate, (mainCounter+1)));
            }
        }
        dayTypeCounter = dayTypeCounter+1;
        if(dayTypeCounter > 3){
            dayTypeCounter = 0;
        }
    }
    console.log(events);
    return events
}

function createEvent(dayType, date, numberOfDay){
    return {
        name:  DAY_TYPES[dayType].displayName,
        displayName: DAY_TYPES[dayType].displayName,
        start: date,
        end: date,
        color: DAY_TYPES[dayType].color,
        timed: true,
        placeNumber: 1,
        numberOfDay: numberOfDay
    };
}
const DAY_TYPES = {
    "protein" : {color: "red", displayName: "Fehérje"},
    "starch" : {color: "orange", displayName: "Keményítő"},
    "carbs": {color: "yellow", displayName: "Szénhidrát"},
    "fruit": {color: "green", displayName: "Vitamin"},
    "water": {color: "light-blue", displayName: "Víz"}
    }

module.export = {
DAYTYPES: DAY_TYPES
}
